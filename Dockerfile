FROM alpine:3.15

RUN apk add --no-cache krb5

WORKDIR /container

ADD docker-entrypoint.sh .

ENTRYPOINT ["./docker-entrypoint.sh"]
