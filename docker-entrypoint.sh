#!/bin/sh

KRB5_PRINCIPAL=${KRB5_PRINCIPAL:-"admin/admin"}
[ -n "$KRB5_PRINCIPAL_PASSWORD_FILE" ] && \
    KRB5_PRINCIPAL_PASSWORD=`cat $KRB5_PRINCIPAL_PASSWORD_FILE`

if [ -z ${KRB5_PRINCIPAL_PASSWORD} ]; then
    echo "No KRB5_PRINCIPAL_PASSWORD Provided. Exiting ..."
    exit 1
fi

if [ -z ${KRB5_REALM} ]; then
    echo "No KRB5_REALM Provided. Exiting ..."
    exit 1
fi

if [ -z ${KRB5_ADMINSERVER} ]; then
    echo "No KRB5_ADMINSERVER Provided. Exiting ..."
    exit 1
fi

if [ -z ${KRB5_KDC} ]; then
    echo "No KRB5_KDC Provided. Exiting ..."
    exit 1
fi

function setup_krb5_config()
{
    cat > /etc/krb5.conf << EOF
[libdefaults]
	default_realm = $KRB5_REALM

[realms]
	$KRB5_REALM = {
		admin_server = $KRB5_ADMINSERVER
		kdc = $KRB5_KDC
	}
EOF
}

function kadm()
{
    kadmin -p "$KRB5_PRINCIPAL" -w "$KRB5_PRINCIPAL_PASSWORD" "$@"
}

function is_kadmin_up()
{
    kadm -q "" > /dev/null 2> /dev/null
}

wait_for_kadmin()
{
    echo "Waiting for kadmin..."
    i=0
    max_err=20

    while [ $i -lt $max_err ]; do
        if is_kadmin_up; then
            return
        fi
        i=$((i + 1))
        sleep 1
    done

    echo "kadmin still not up, aborting..."
    exit 1
}

function setup_services()
{
    mkdir -p /container/keytabs
    echo "Creating services..."
    for service in $SERVICES; do
        principals=`eval echo $\`echo $service | tr '[:lower:]' '[:upper:]'\`_PRINCIPAL`
        if [ -z "$principals" ]; then
            echo "$service do not have principals"
            continue
        fi

        for principal in $principals; do
            kadm -q "addprinc -randkey $principal"
            kadm -q "ktadd -k /container/keytabs/${service}.keytab.tmp $principal"
        done

        # This trick enables you to bind the keytab in the container
        cat /container/keytabs/${service}.keytab.tmp > /container/keytabs/${service}.keytab
        rm -f /container/keytabs/${service}.keytab.tmp
    done
}

setup_krb5_config
wait_for_kadmin
setup_services
